module Brandscopic
  module Utils
    class CustomFiltersImport
      attr_accessor :errors, :column_index, :options
      APPLY_TO_EQUIVALENCES = { 'Events'                    => 'events',
                                'Venues'                    => 'venues',
                                'Tasks'                     => 'tasks',
                                'Brand Ambassadors Visits'  => 'visits',
                                'Users'                     => 'company_users',
                                'Teams'                     => 'teams',
                                'Roles'                     => 'roles',
                                'Campaigns'                 => 'campaigns',
                                'Brands'                    => 'brands',
                                'Activity Types'            => 'activity_types',
                                'Areas'                     => 'areas',
                                'Brand Portfolios'          => 'brand_portfolios',
                                'Date Ranges'               => 'date_ranges',
                                'Day Parts'                 => 'day_parts',
                                'Results Event Data'        => 'event_data',
                                'Results Activity Data'     => 'activities',
                                'Results Comments'          => 'results_comments',
                                'Results Expenses'          => 'results_expenses',
                                'Results Media Gallery'     => 'results_photos',
                                'Results Surveys'           => 'surveys',
                                'Campaign Summary'          => 'campaign_summary_report' }

      # Receives a file handler. This allows to send the $stdin from the
      # rake file. The file loaded in an array to allow go through
      # it more than once
      def initialize(file, options = {})
        @rows = []
        opts = { row_sep: "\n", col_sep: ',', skip_blanks: true }.merge(options)
        CSV(file, opts) do |csv|
          csv.each do |r|
            @rows.push(r)
          end
        end
        @column_index = {
          company: 0, email: 1, filter_name: 2, section: 3, campaigns: 4, brands: 5,
          brand_portfolios: 6, areas: 7, users: 8, teams: 9, cities: 10, event_status: 11,
          active_state: 12, star_rating: 13
        }
      end

      def run!
        new_filters = []
        @rows.each_with_index do |row, i|
          row_filters, row_errors = [], []
          company, email, filter_name, section, campaigns, brands, brand_portfolios, areas,
          users, teams, cities, event_status, active_state, star_rating = split_row(row)

          # Getting the company
          if !the_company = Company.where(name: company).first
            @errors.push(line: i + 1, error: "The company \"#{company}\" for the user with the email address \"#{email}\" don't exist.")
            next
          end

          # Verifying the user existence
          if !the_user = User.where(["lower(users.email) = '%s'", email]).first
            @errors.push(line: i + 1, error: "The user with the email address #{email} don't exist.")
            next
          elsif !the_company_user = the_user.company_users.select { |cu| cu.company_id == the_company.id }.first
            @errors.push(line: i + 1, error: "The user with the email address #{email} for company #{company} don't exist.")
            next
          end

          # Verifying sections existence
          the_sections = []
          section.split(',').map(&:strip).each do |s|
            if !section = APPLY_TO_EQUIVALENCES[s]
              row_errors.push(line: i + 1, error: "The section \"#{s}\" for the user with the email address \"#{email}\" don't exist.")
            else
              the_sections.push(section)
            end
          end

          # Getting the campaigns filters
          campaigns.split(',').map(&:strip).each do |campaign|
            if !the_campaign = Campaign.where(name: campaign, company: the_company).first
              row_errors.push(line: i + 1, error: "The filter campaign \"#{campaign}\" for the user with the email address \"#{email}\" don't exist.")
            else
              row_filters.push("campaign%5B%5D=#{the_campaign.id}")
            end
          end if campaigns.present?

          # Getting the brands filters
          brands.split(',').map(&:strip).each do |brand|
            if !the_brand = Brand.where(name: brand, company: the_company).first
              row_errors.push(line: i + 1, error: "The filter brand \"#{brand}\" for the user with the email address \"#{email}\" don't exist.")
            else
              row_filters.push("brand%5B%5D=#{the_brand.id}")
            end
          end if brands.present?

          # Getting the brand portfolios filters
          brand_portfolios.split(',').map(&:strip).each do |brand_portfolio|
            if !the_brand_portfolio = BrandPortfolio.where(name: brand_portfolio, company: the_company).first
              row_errors.push(line: i + 1, error: "The filter brand portfolio \"#{brand_portfolio}\" for the user with the email address \"#{email}\" don't exist.")
            else
              row_filters.push("brand_portfolio%5B%5D=#{the_brand_portfolio.id}")
            end
          end if brand_portfolios.present?

          # Getting the areas filters
          areas.split(',').map(&:strip).each do |area|
            if !the_area = Area.where(name: area, company: the_company).first
              row_errors.push(line: i + 1, error: "The filter area \"#{area}\" for the user with the email address \"#{email}\" don't exist.")
            else
              row_filters.push("area%5B%5D=#{the_area.id}")
            end
          end if areas.present?

          # Getting the users filters
          users.split(',').map(&:strip).each do |user|
            if !the_user = the_company.company_users.select { |cu| cu.name == user }.first
              row_errors.push(line: i + 1, error: "The filter user \"#{user}\" for the user with the email address \"#{email}\" don't exist.")
            else
              row_filters.push("user%5B%5D=#{the_user.id}")
            end
          end if users.present?

          # Getting the teams filters
          teams.split(',').map(&:strip).each do |team|
            if !the_team = Team.where(name: team, company: the_company).first
              row_errors.push(line: i + 1, error: "The filter team \"#{team}\" for the user with the email address \"#{email}\" don't exist.")
            else
              row_filters.push("team%5B%5D=#{the_team.id}")
            end
          end if teams.present?

          # Getting the cities filters
          cities.split(',').map(&:strip).each do |city|
            if the_company.brand_ambassadors_visits.all.pluck(:city).uniq.reject!(&:blank?).exclude?(city)
              row_errors.push(line: i + 1, error: "The filter city \"#{city}\" for the user with the email address \"#{email}\" don't exist.")
            else
              row_filters.push("city%5B%5D=#{city}")
            end
          end if cities.present?

          # Getting the events status filters
          event_status.split(',').map(&:strip).each do |status|
            if %w(Approved Due Late Rejected Scheduled Submitted).exclude?(status)
              row_errors.push(line: i + 1, error: "The filter event status \"#{status}\" for the user with the email address \"#{email}\" don't exist.")
            else
              row_filters.push("event_status%5B%5D=#{status}")
            end
          end if event_status.present?

          # Getting the active state filters
          active_state.split(',').map(&:strip).each do |status|
            if %w(Active Inactive).exclude?(status)
              row_errors.push(line: i + 1, error: "The filter active state \"#{status}\" for the user with the email address \"#{email}\" don't exist.")
            else
              row_filters.push("status%5B%5D=#{status}")
            end
          end if active_state.present?

          # Getting the star rating filters
          star_rating.split(',').map(&:strip).each do |rating|
            if %w(0 1 2 3 4 5).exclude?(rating)
              row_errors.push(line: i + 1, error: "The filter star rating \"#{rating}\" for the user with the email address \"#{email}\" don't exist.")
            else
              row_filters.push("rating%5B%5D=#{rating}")
            end
          end if star_rating.present?

          if row_errors.present?
            @errors += row_errors
          else
            the_sections.each do |s|
              the_filter = row_filters.join('&')
              if CustomFilter.where(owner_id: the_company_user.id, apply_to: s, filters: the_filter).first
                @errors.push(line: i + 1, error: "The filter \"#{the_filter}\" in the section \"#{s}\" for the user with the email address \"#{email}\" already exists.")
              else
                custom_filter = CustomFilter.new
                custom_filter.name = filter_name
                custom_filter.apply_to = s
                custom_filter.filters = the_filter
                custom_filter.owner_id = the_company_user.id
                custom_filter.owner_type = 'CompanyUser'
                custom_filter.default_view = true
                custom_filter.category_id = nil
                if custom_filter.save
                  new_filters.push(line: i + 1, error: "Added: Filter \"#{the_filter}\" in the section \"#{s}\" for the user with the email address \"#{email}\".")
                else
                  @errors.push(line: i + 1, error: "Failed: Filter \"#{the_filter}\" in the section \"#{s}\" for the user with the email address \"#{email}\".")
                end
              end
            end
          end
        end

        File.open('tmp/imported_custom_filters.txt', 'w') do |f|
          f.puts new_filters.map { |e| "[Line ##{e[:line]}] #{e[:error]}" }.join("\n")
        end if new_filters.present?

        @errors.empty?
      end

      def validate
        @errors = []
        @rows.each_with_index do |row, i|
          if row[1].blank?
            @errors.push(line: i + 1, error: 'The email address should be specified.')
            next
          end

          if row[0].blank?
            @errors.push(line: i + 1, error: "The filter company for the user with the email address \"#{row[1]}\" should be specified.")
            next
          end

          if row[2].blank?
            @errors.push(line: i + 1, error: "The filter name for the user with the email address \"#{row[1]}\" should be specified.")
            next
          end

          if row[3].blank?
            @errors.push(line: i + 1, error: "The filter section for the user with the email address \"#{row[1]}\" should be specified.")
            next
          end

          if row[4..13].all?(&:blank?)
            @errors.push(line: i + 1, error: "At least on filter criteria for the user with the email address \"#{row[1]}\" should be specified.")
            next
          end
        end
        @errors.empty?
      end

      def split_row(row)
        @column_index.map { |_k, i| row[i]  }
      end

      def logger
        Rails.logger
      end
    end
  end
end
