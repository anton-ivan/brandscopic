Geocoder.configure(

  # geocoding service (see below for supported options):
  lookup: :google,

  # to use an API key:
  api_key: ::GOOGLE_API_KEY,

  # geocoding service by IP
  freegeoip: { host: 'freegeoip.co' },
  ip_lookup: :freegeoip,

  # geocoding service request timeout, in seconds (default 3):
  timeout: 5,

  # set default units to kilometers:
  units: :km,

  # define raise our own exceptions for certain events
  always_raise: [Timeout::Error]
)
