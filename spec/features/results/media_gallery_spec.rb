require 'rails_helper'

feature 'Results Media Gallery Page', js: true, search: true do
  let(:user) { create(:user, company_id: create(:company).id, role_id: create(:role).id) }
  let(:company) { user.companies.first }
  let(:company_user) { user.current_company_user }
  let(:campaign) { create(:campaign, company: company, modules: { 'photos' => {} }) }
  let(:event) { create(:late_event, company: company, campaign: campaign) }

  before do
    Warden.test_mode!
    sign_in user
    Kpi.create_global_kpis
  end

  after do
    AttachedAsset.destroy_all
    Warden.test_reset!
  end

  feature 'Media Gallery Results', js: true do
    scenario 'a user can see photos from Events Galleries, PERs and Activities in the photos report' do
      activity_type = create(:activity_type, name: 'Activity Type #1', company: company)
      campaign.activity_types << activity_type
      activity = create(:activity, activity_type: activity_type, activitable: event, campaign: campaign, company_user_id: 1)

      ff_photo = FormField.find(create(:form_field, fieldable: activity_type, type: 'FormField::Photo').id)
      activity_result = FormFieldResult.new(form_field: ff_photo, resultable: activity)

      ff_photo = FormField.find(create(:form_field, fieldable: campaign, type: 'FormField::Photo').id)
      event_result = FormFieldResult.new(form_field: ff_photo, resultable: event)

      create(:photo, attachable: activity_result, file_file_name: 'activity_photo.jpg', active: true)
      create(:photo, attachable: event_result, file_file_name: 'per_photo.jpg', active: true)
      create(:photo, attachable: event, file_file_name: 'event_photo.jpg', active: true)
      Sunspot.commit

      visit results_photos_path

      expect(page).to have_selector('#photos-list .photo-item', count: 3)
      within gallery_box do
        expect(page).to have_css("img[alt='Activity photo']")
        expect(page).to have_css("img[alt='Per photo']")
        expect(page).to have_css("img[alt='Event photo']")
      end
    end

    scenario 'a user can activate a photo' do
      # This should be done from Photo Results section
      create(:photo, attachable: event, active: false)
      Sunspot.commit

      visit results_photos_path

      filter_section('ACTIVE STATE').unicheck('Inactive')

      # Check that the image appears on the page
      within gallery_box do
        expect(page).to have_selector('li')
        click_js_link 'View Photo'
      end

      # Activate the image from the link inside the gallery modal
      within gallery_modal do
        find('.slider').hover
        within '.slider' do
          click_js_link 'Activate'
          expect(page).not_to have_link('Activate')
          expect(page).to have_link('Deactivate')
        end
      end
    end

    scenario 'an user can mark one or more photos/videos and download them' do
      create(:photo, attachable: event, created_at: Time.zone.local(2015, 8, 23, 11, 59))
      create(:video, attachable: event, created_at: Time.zone.local(2015, 8, 22, 10, 30))
      Sunspot.commit

      visit results_photos_path

      within '#download-photos-counter-container' do
        expect(page).to have_no_selector('.download-photos-counter')
      end

      click_js_button 'Download'
      confirm_prompt 'Please select at least one photo to download'

      within gallery_box do
        # Select a photo to download
        photo = gallery_item 1
        expect(photo['class']).to_not include('selected-for-download')
        within photo do
          click_js_link 'Select Photo'
          expect(photo['class']).to include('selected-for-download')
        end

        # Select a video to download
        video = gallery_item 2
        expect(video['class']).to_not include('selected-for-download')
        within video do
          click_js_link 'Select Video'
          expect(video['class']).to include('selected-for-download')
        end
      end

      within '#download-photos-counter-container' do
        expect(page).to have_selector('.download-photos-counter', text: '2')
      end

      click_js_button 'Download'
      expect(page).to have_content('We are packaging the photos for you, the download will start soon...')
    end
  end

  def gallery_box
    page.find('.gallery.photoGallery')
  end

  def gallery_modal
    page.find('.gallery-modal')
  end

  def gallery_item(resource = 1)
    item = page.find(".photo-item:nth-child(#{resource})")
    begin
      item.hover
    rescue Capybara::Poltergeist::MouseEventFailed
      page.evaluate_script 'window.scrollBy(0, -100);'
      item.hover
    end
    item
  end
end
